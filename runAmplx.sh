#!/bin/bash
export AMPLX_DIR=$(dirname $0)
export PYTHONPATH=$AMPLX_DIR
echo $AMPLX_DIR
python $AMPLX_DIR/update_token.py -t $AMPLX_DIR/.token
export ampl_include=$'.\n'$AMPLX_DIR
export AMPLX_SOLVE_RUNNER=$AMPLX_DIR/amplx_slist_solve_runner2.py
ampl amplx.amp | tee amplx.out.log.txt
echo $? > amplx.out.status.txt

