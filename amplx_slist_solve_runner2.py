import os
import sys
import everest
from zipfile import ZipFile
import amplx_config

solver = sys.argv[1]
delim = sys.argv.index('--')
opts = ''
if delim > 2:
    opts = sys.argv[2]
if opts == '':
    with open('zero.txt', 'w'):
        pass
    opts = 'zero.txt'
taskFiles = sys.argv[(delim + 1):]

tokenFile = '.token'
if 'AMPLX_DIR' in os.environ:
    tokenFile = os.path.join(os.environ['AMPLX_DIR'], '.token')

with open(tokenFile, 'r') as f:
    token = f.read().strip()

session = everest.Session('amplx', 'https://everest.distcomp.org', token = token)

try:
    amplApp = everest.App(amplx_config.SOLVE_AMPL_STUB_ID, session)
    jobs = {}
    for x in taskFiles:
        job = amplApp.run({
            "optionsString": "",
            "optionsFile": open(opts, 'r'),
            "solver": solver,
            "stub": open(x + '.nl', 'r')
        }, [])
        jobs[job] = x
    for j, f in jobs.iteritems():
        result = j.result()
        session.getFile(result['solution'], f + '.sol')
        session.getFile(result['solve-ampl-stub-log'], f + '.log')
finally:
    session.close()

print "Done"
