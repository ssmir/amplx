import os
import sys
import everest
from zipfile import ZipFile
import amplx_config

solver = sys.argv[1]
stub = sys.argv[2]

session = everest.Session(
    'amplx',
    'https://everest.distcomp.org',
    token = amplx_config.TOKEN
)
try:
    amplApp = everest.App(amplx_config.SOLVE_AMPL_STUB_ID, session)
    jobs = []
    for i in range(int(sys.argv[3])):
        job = amplApp.run({
            "optionsString": "",
            "solver": solver,
            "stub": open(stub, 'r')
        }, [])
        jobs.append(job)
    print('%d jobs submitted' % len(jobs))
    for i in range(len(jobs)):
        result = jobs[i].result()
        session.getFile(result['solution'], '%d.sol' % i)
        session.getFile(result['solve-ampl-stub-log'], '%d.log.txt' % i)
        print('Solution and log written for job %d' % i)
finally:
    session.close()

print "Done"
