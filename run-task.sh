#!/bin/bash
set -e
SOLVER="$1"
N="$2"
OPTS=""
case "$SOLVER" in
    scipamplx)
        OPTS=options.txt
        ;;
    scipampl)
        OPTS=options.txt
        ;;
    *)
        OPTS=$(cat options.txt)
        ;;
esac

$SOLVER stub${N}.nl -AMPL $OPTS > stdout${N}.txt 2> stderr${N}.txt
