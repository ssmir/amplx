import os
import sys
import everest
from zipfile import ZipFile
import amplx_config

solver = sys.argv[1]
delim = sys.argv.index('--')
opts = ''
if delim > 2:
    opts = sys.argv[2]
taskFiles = {}
i = 1
for x in sys.argv[(delim + 1):]:
    zipName = 'stub%d' % i
    taskFiles[zipName] = x
    i += 1
if opts == '':
    with open('zero.txt', 'w'):
        pass
    opts = 'zero.txt'

tokenFile = '.token'
if 'AMPLX_DIR' in os.environ:
    tokenFile = os.path.join(os.environ['AMPLX_DIR'], '.token')

with open(tokenFile, 'r') as f:
    token = f.read().strip()

session = everest.Session('amplx', 'https://everest.distcomp.org', token = token)

try:
    sweep = everest.App(amplx_config.PARAMETER_SWEEP_ID, session)
    runner = os.path.join(os.path.split(sys.argv[0])[0], 'run-task.sh')
    z = ZipFile('stubs.zip', 'w')
    try:
        z.write(opts, 'options.txt')
        z.write(runner, 'run-task.sh')
        for zipName, extName  in taskFiles.iteritems():
            z.write(extName + '.nl', zipName + '.nl')
    finally:
        z.close()
    with open('plan.txt', 'wb') as f:
        f.write('parameter n from 1 to %d step 1\n' % len(taskFiles))
        f.write('input_files run-task.sh options.txt stub${n}.nl\n')
        f.write('command bash run-task.sh %s ${n}\n' % solver)
        f.write('output_files stub${n}.sol stderr${n}.txt stdout${n}.txt\n')
    job = sweep.run({
        "plan": open('plan.txt', 'rb'),
        "files": open('stubs.zip', 'rb')
    }, amplx_config.RESOURCES)
    result = job.result()
    session.getFile(result['results'], 'results.zip')
    z = ZipFile('results.zip', 'r')
    try:
        for x in z.namelist():
            if 'stub' in x:
                sol = z.read(x)
                prefix = x.split('/')[-1].split('.')[0]
                solName = taskFiles[prefix] + '.sol'
                print('Writing %s' % solName)
                with open(solName, 'wb') as f:
                    f.write(sol)
    finally:
        z.close()
finally:
    session.close()

print "Done"
