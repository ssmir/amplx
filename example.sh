#!/bin/bash
set -e
#export AMPLX_SOLVER=lpsolve
#export AMPLX_SOLVER=scipamplx
export AMPLX_SOLVER=scip 
#export AMPLX_SOLVER=ipopt
export ampl_include=$'.\nmulti_test'
export AMPLX_SOLVE_RUNNER=amplx_slist_solve_runner2.py
python update_token.py -t .token
time ampl multi_test/multi2_mclTest.amp
